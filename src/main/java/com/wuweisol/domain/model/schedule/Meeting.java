/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.schedule;

import java.io.Serializable;
import java.util.*;

import org.joda.time.Interval;

/**
 *
 * @author Charles
 */
public class Meeting<E extends SchedulableEntity> extends Appointment
{
   private static final long serialVersionUID = 1L;
   private Set<E> participants;

   public Meeting()
   {
   }

   public Meeting(String displayName, Interval interval)
   {
      super(displayName, interval);
   }

   public Meeting(String displayName, Interval interval, String location)
   {
      super(displayName, interval, location);
   }

   public Meeting(String displayName, Interval interval, String location, String description)
   {
      super(displayName, interval, location, description);
   }

   public Meeting(String displayName, Interval interval, Set<E> participants)
   {
      this.participants = participants;
   }

   public Meeting(String displayName, Interval interval, E... participants)
   {
      this(displayName, interval);
      this.participants = new LinkedHashSet<>(Arrays.asList(participants));
   }

   public Set<E> getParticipants()
   {
      if (participants == null)
      {
         participants = new LinkedHashSet<>();
      }
      return participants;
   }

   public void setParticipants(Set<E> participants)
   {
      this.participants = participants;
   }

   public Set<E> getOtherParticipants(E knownParticipant)
   {
      Set<E> otherParticipants = new LinkedHashSet<>(getParticipants());
      otherParticipants.remove(knownParticipant);
      return otherParticipants;
   }

   @Override
   public String toString()
   {
      return "Meeting{" +
            "participants=" + participants +
            "} " + super.toString();
   }
   
   
}
