/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.schedule;

import org.apache.commons.collections.comparators.NullComparator;
import org.joda.time.Interval;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author Charles
 */
public class Appointment implements Serializable
{
   public static final Comparator<Appointment> START_TIME_COMPARATOR = new NullComparator(new Comparator<Appointment>()
   {
      private Comparator<Interval> intervalComparator = new NullComparator(new Comparator<Interval>()
      {
         @Override
         public int compare(Interval o1, Interval o2)
         {
            // An interval's start/end times cannot be null.
            return o1.getStart().compareTo(o2.getStart());
         }
      }, false);
      
      @Override
      public int compare(Appointment o1, Appointment o2)
      {
         return intervalComparator.compare(o1.getInterval(), o2.getInterval());
      }
   }, false);
   
   public static final Comparator<Appointment> END_TIME_COMPARATOR = new NullComparator(new Comparator<Appointment>()
   {
      private Comparator<Interval> intervalComparator = new NullComparator(new Comparator<Interval>()
      {
         @Override
         public int compare(Interval o1, Interval o2)
         {
            // An interval's start/end times cannot be null.
            return o1.getEnd().compareTo(o2.getEnd());
         }
      }, false);
      
      @Override
      public int compare(Appointment o1, Appointment o2)
      {
         return intervalComparator.compare(o1.getInterval(), o2.getInterval());
      }
   }, false);
   
   private static final long serialVersionUID = 1L;
   private Long id;
   private String displayName;
   private Interval interval;
   private String location;
   private String description;

   public Appointment()
   {
   }

   public Appointment(String displayName, Interval interval)
   {
      this.displayName = displayName;
      this.interval = interval;
   }

   public Appointment(String displayName, Interval interval, String location)
   {
      this.displayName = displayName;
      this.interval = interval;
      this.location = location;
   }

   public Appointment(String displayName, Interval interval, String location, String description)
   {
      this.displayName = displayName;
      this.interval = interval;
      this.location = location;
      this.description = description;
   }
   
   public String getDisplayName()
   {
      return displayName;
   }

   public void setDisplayName(String displayName)
   {
      this.displayName = displayName;
   }

   public Long getId()
   {
      return id;
   }

   public void setId(Long id)
   {
      this.id = id;
   }

   public Interval getInterval()
   {
      return interval;
   }

   public void setInterval(Interval interval)
   {
      this.interval = interval;
   }

   public String getLocation()
   {
      return location;
   }

   public void setLocation(String location)
   {
      this.location = location;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   @Override
   public String toString()
   {
      return "ScheduleEvent{" + "id=" + id + ", displayName=" + displayName + ", interval=" + interval + ", location=" + location + ", description=" + description + '}';
   }

   @Override
   public boolean equals(Object o)
   {
      if(this == o) return true;
      if(!(o instanceof Appointment)) return false;

      Appointment that = (Appointment) o;

      if(description != null ? !description.equals(that.description) : that.description != null) return false;
      if(displayName != null ? !displayName.equals(that.displayName) : that.displayName != null) return false;
      if(id != null ? !id.equals(that.id) : that.id != null) return false;
      if(interval != null ? !interval.equals(that.interval) : that.interval != null) return false;
      if(location != null ? !location.equals(that.location) : that.location != null) return false;

      return true;
   }

   @Override
   public int hashCode()
   {
      int result = id != null ? id.hashCode() : 0;
      result = 31 * result + (displayName != null ? displayName.hashCode() : 0);
      result = 31 * result + (interval != null ? interval.hashCode() : 0);
      result = 31 * result + (location != null ? location.hashCode() : 0);
      result = 31 * result + (description != null ? description.hashCode() : 0);
      return result;
   }
}
