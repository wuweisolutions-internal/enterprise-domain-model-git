/*
 * Copyright (c) 2012, Charles W. Stanton, Aaron R. Filonowich
 * All rights reserved.
 *
 * The right to redistribute and use in source and in binary forms, with or without modifications,
 * is granted solely to the Global Marketing Development Center (GMDC) and to any individuals or
 * organizations in possession of written approval from GMDC.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.wuweisol.domain.model.schedule;

import com.wuweisol.domain.util.Lists;
import java.io.Serializable;
import java.util.*;

import org.joda.time.DateTime;
import org.joda.time.Interval;

/**
 *
 * @author Charles
 */
public class Schedule implements Serializable
{
   private static final Lists.Matcher<Appointment> HAS_START_TIME_MATCHER = new Lists.Matcher<Appointment>()
   {
      @Override
      public boolean matches(Appointment value)
      {
         return (value != null && value.getInterval() != null && value.getInterval().getStart() != null);
      }
   };

   private static final Lists.Matcher<Appointment> HAS_END_TIME_MATCHER = new Lists.Matcher<Appointment>()
   {
      @Override
      public boolean matches(Appointment value)
      {
         return (value != null && value.getInterval() != null && value.getInterval().getEnd() != null);
      }
   };

   private class AppointmentIntervalContainedWithinBoundingIntervalMatcher implements Lists.Matcher<Appointment>
   {
      private Interval boundingInterval;

      public AppointmentIntervalContainedWithinBoundingIntervalMatcher(Interval boundingInterval)
      {
         this.boundingInterval = boundingInterval;
      }

      @Override
      public boolean matches(Appointment value)
      {
         if (value != null && value.getInterval() != null && boundingInterval.contains(value.getInterval()))
         {
            return true;
         }
         return false;
      }
   }

   private static final long serialVersionUID = 1L;
   private Long id;
   private List<Appointment> appointments;
   private SchedulableParty scheduleParty;

   public Schedule()
   {
   }

   public Schedule(List<Appointment> appointments)
   {
      this.appointments = appointments;
   }

   public SchedulableParty getScheduleParty()
   {
      return scheduleParty;
   }

   public void setScheduleParty(SchedulableParty scheduleParty)
   {
      this.scheduleParty = scheduleParty;
   }

   public Long getId()
   {
      return id;
   }

   public void setId(Long id)
   {
      this.id = id;
   }

   public List<Appointment> getAppointments()
   {
      if (appointments == null)
      {
         appointments = new ArrayList<>();
      }
      return appointments;
   }

   public void setAppointments(List<Appointment> appointments)
   {
      this.appointments = appointments;
   }

   public List<Appointment> getAppointmentsWithin(Interval interval)
   {
      Collections.sort(getAppointments(), Appointment.START_TIME_COMPARATOR);
      return Lists.allMatches(getAppointments(), new AppointmentIntervalContainedWithinBoundingIntervalMatcher(interval));
   }

   public DateTime getEarliestStartTime()
   {
      return getFirstStartTime(Appointment.START_TIME_COMPARATOR, HAS_START_TIME_MATCHER);
   }

   public DateTime getLatestStartTime()
   {
      return getLastStartTime(Appointment.START_TIME_COMPARATOR, HAS_START_TIME_MATCHER);
   }

   public DateTime getEarliestEndTime()
   {
      return getFirstStartTime(Appointment.END_TIME_COMPARATOR, HAS_END_TIME_MATCHER);
   }

   public DateTime getLatestEndTime()
   {
      return getLastStartTime(Appointment.END_TIME_COMPARATOR, HAS_END_TIME_MATCHER);
   }

   public Appointment getFirstAppointment(Comparator<Appointment> comparator, Lists.Matcher<Appointment> matcher)
   {
      Collections.sort(getAppointments(), comparator);
      return Lists.firstMatch(getAppointments(), matcher);
   }

   public Appointment getLastAppointment(Comparator<Appointment> comparator, Lists.Matcher<Appointment> matcher)
   {
      Collections.sort(getAppointments(), comparator);
      return Lists.lastMatch(getAppointments(), matcher);
   }

   public DateTime getFirstStartTime(Comparator<Appointment> comparator, Lists.Matcher<Appointment> matcher)
   {
      Appointment firstAppointment = getFirstAppointment(comparator, matcher);
      if (firstAppointment != null)
      {
         return firstAppointment.getInterval().getStart();
      }
      return null;
   }

   public DateTime getLastStartTime(Comparator<Appointment> comparator, Lists.Matcher<Appointment> matcher)
   {
      Appointment lastAppointment = getLastAppointment(comparator, matcher);
      if (lastAppointment != null)
      {
         return lastAppointment.getInterval().getStart();
      }
      return null;
   }

   public void addAll(Schedule other)
   {
      Set<Appointment> distinctAppointments = new HashSet<>();
      distinctAppointments.addAll(this.getAppointments());
      distinctAppointments.addAll(other.getAppointments());
      this.getAppointments().clear();
      this.getAppointments().addAll(distinctAppointments);
      Collections.sort(this.getAppointments(), Appointment.START_TIME_COMPARATOR);
   }

   public void removeAll(Schedule other)
   {
      this.getAppointments().removeAll(other.getAppointments());
   }

   protected void sortAppointmentsByStartTime()
   {
   }

   @Override
   public int hashCode()
   {
      int hash = 3;
      hash = 67 * hash + Objects.hashCode(this.id);
      return hash;
   }

   @Override
   public boolean equals(Object obj)
   {
      if (obj == null)
      {
         return false;
      }
      if (getClass() != obj.getClass())
      {
         return false;
      }
      final Schedule other = (Schedule) obj;
      if (!Objects.equals(this.id, other.id))
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      return "Schedule{" + "id=" + id + ", activities=" + appointments + '}';
   }
}
