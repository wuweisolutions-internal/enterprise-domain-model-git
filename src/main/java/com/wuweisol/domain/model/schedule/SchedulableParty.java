/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.schedule;

import com.wuweisol.domain.model.party.Party;
import java.util.Objects;

/**
 *
 * @author Charles
 */
public class SchedulableParty<P extends Party> extends SchedulableEntity
{
   private P party;

   public SchedulableParty()
   {
   }

   public SchedulableParty(P party)
   {
      this.party = party;
   }

   public P getParty()
   {
      return party;
   }

   public void setParty(P party)
   {
      this.party = party;
   }

   @Override
   public int hashCode()
   {
      int hash = 7;
      hash = 23 * hash + Objects.hashCode(this.party);
      return hash;
   }

   @Override
   public boolean equals(Object obj)
   {
      if (obj == null)
      {
         return false;
      }
      if (getClass() != obj.getClass())
      {
         return false;
      }
      final SchedulableParty<P> other = (SchedulableParty<P>) obj;
      if (!Objects.equals(this.party, other.party))
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      return "SchedulableParty{" + "party=" + party + '}';
   }

}
