/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.schedule;

import java.io.Serializable;

/**
 *
 * @author Charles
 */
public abstract class SchedulableEntity implements Serializable
{
   private static final long serialVersionUID = 1L;
   
   private Long id;
   private Schedule schedule;

   public SchedulableEntity()
   {
   }

   public Long getId()
   {
      return id;
   }

   public void setId(Long id)
   {
      this.id = id;
   }

   public Schedule getSchedule()
   {
      return schedule;
   }

   public void setSchedule(Schedule schedule)
   {
      this.schedule = schedule;
   }
}
