/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.property;

import java.io.Serializable;

/**
 * @author Charles
 */
public class Property implements Serializable
{
   private static final long serialVersionUID = 1L;
   private Long id;
   private String propertyName;

   public Property()
   {
   }

   public String getPropertyName()
   {
      return propertyName;
   }

   public void setPropertyName(String propertyName)
   {
      this.propertyName = propertyName;
   }

   public Long getId()
   {
      return id;
   }

   public void setId(Long id)
   {
      this.id = id;
   }

   @Override
   public int hashCode()
   {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object)
   {
      // TODO: Warning - this method won't work in the case the id fields are not set
      if(!(object instanceof Property))
      {
         return false;
      }
      Property other = (Property) object;
      if((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      return "com.wuweisol.model.Property[ id=" + id + " ]";
   }
}
