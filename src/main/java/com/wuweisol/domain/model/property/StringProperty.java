/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.property;

/**
 * @author Charles
 */
public class StringProperty extends Property
{
   private String propertyValue;

   public StringProperty()
   {
   }

   public String getPropertyValue()
   {
      return propertyValue;
   }

   public void setPropertyValue(String propertyValue)
   {
      this.propertyValue = propertyValue;
   }
}
