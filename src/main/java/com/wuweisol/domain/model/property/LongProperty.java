/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.property;

/**
 * @author Charles
 */
public class LongProperty extends Property
{
   private Long propertyValue;

   public LongProperty()
   {
   }

   public Long getPropertyValue()
   {
      return propertyValue;
   }

   public void setPropertyValue(Long propertyValue)
   {
      this.propertyValue = propertyValue;
   }

   @Override
   public boolean equals(Object object)
   {
      return super.equals(object);
   }

   @Override
   public int hashCode()
   {
      return super.hashCode();
   }

}
