/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.party;

import com.wuweisol.domain.model.property.StringProperty;

import java.util.Random;

/**
 * @author Charles
 */
public class Person extends Party
{
   public static final String PLACE_OF_BIRTH = "Person.PLACE_OF_BIRTH";
   private static final long serialVersionUID = 1L;

   private PersonName personName;

   public Person()
   {
   }

   public PersonName getPersonName()
   {
      return personName;
   }

   public void setPersonName(PersonName personName)
   {
      this.personName = personName;
   }

   public String getPlaceOfBirth()
   {
      return getStringProperties().get(PLACE_OF_BIRTH).getPropertyValue();
   }

   public void setPlaceOfBirth(String value)
   {
      StringProperty property = getStringProperties().get(PLACE_OF_BIRTH);
      if(property == null)
      {
         property = new StringProperty();
         property.setId(new Random().nextLong());
         property.setPropertyName(PLACE_OF_BIRTH);
         getStringProperties().put(PLACE_OF_BIRTH, property);
      }
      property.setPropertyValue(value);
   }

}
