/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.party;

/**
 * @author Charles
 */
public class EmailAddress
      extends Address
{
   private String address;

   /**
    *
    */
   public EmailAddress()
   {
      super();
   }

   public String getAddress()
   {
      return address;
   }

   public void setAddress(String address)
   {
      this.address = address;
   }

   @Override
   public String toString()
   {
      return "EmailAddress{" +
            "address='" + address + '\'' +
            '}';
   }

   @Override
   public boolean equals(Object o)
   {
      if(this == o) return true;
      if(!(o instanceof EmailAddress)) return false;
      if(!super.equals(o)) return false;

      EmailAddress that = (EmailAddress) o;

      if(address != null ? !address.equals(that.address) : that.address != null) return false;

      return true;
   }

   @Override
   public int hashCode()
   {
      int result = super.hashCode();
      result = 31 * result + (address != null ? address.hashCode() : 0);
      return result;
   }
}
