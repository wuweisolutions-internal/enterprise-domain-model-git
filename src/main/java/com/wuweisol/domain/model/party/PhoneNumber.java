package com.wuweisol.domain.model.party;

/**
 * Created with IntelliJ IDEA.
 * User: Charles
 * Date: 8/30/12
 * Time: 10:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class PhoneNumber
      extends Address
{
   private static final long serialVersionUID = 8695089034380011430L;
   
   /**
    *
    */
   private String countryCode = null;

   /**
    *
    */
   private String nationalDirectDialingPrefix = null;

   /**
    *
    */
   private String areaCode = null;

   /**
    *
    */
   private String number = null;

   /**
    *
    */
   private String extension = null;

   public PhoneNumber()
   {
   }

   public String getCountryCode()
   {
      return countryCode;
   }

   public void setCountryCode(String countryCode)
   {
      this.countryCode = countryCode;
   }

   public String getNationalDirectDialingPrefix()
   {
      return nationalDirectDialingPrefix;
   }

   public void setNationalDirectDialingPrefix(String nationalDirectDialingPrefix)
   {
      this.nationalDirectDialingPrefix = nationalDirectDialingPrefix;
   }

   public String getAreaCode()
   {
      return areaCode;
   }

   public void setAreaCode(String areaCode)
   {
      this.areaCode = areaCode;
   }

   public String getNumber()
   {
      return number;
   }

   public void setNumber(String number)
   {
      this.number = number;
   }

   public String getExtension()
   {
      return extension;
   }

   public void setExtension(String extension)
   {
      this.extension = extension;
   }

   @Override
   public String toString()
   {
      return "PhoneNumber{" +
            "countryCode='" + countryCode + '\'' +
            ", nationalDirectDialingPrefix='" + nationalDirectDialingPrefix + '\'' +
            ", areaCode='" + areaCode + '\'' +
            ", number='" + number + '\'' +
            ", extension='" + extension + '\'' +
            "} " + super.toString();
   }

   @Override
   public boolean equals(Object o)
   {
      if(this == o) return true;
      if(!(o instanceof PhoneNumber)) return false;
      if(!super.equals(o)) return false;

      PhoneNumber that = (PhoneNumber) o;

      if(areaCode != null ? !areaCode.equals(that.areaCode) : that.areaCode != null) return false;
      if(countryCode != null ? !countryCode.equals(that.countryCode) : that.countryCode != null) return false;
      if(extension != null ? !extension.equals(that.extension) : that.extension != null) return false;
      if(nationalDirectDialingPrefix != null ? !nationalDirectDialingPrefix.equals(that.nationalDirectDialingPrefix) : that.nationalDirectDialingPrefix != null)
         return false;
      if(number != null ? !number.equals(that.number) : that.number != null) return false;

      return true;
   }

   @Override
   public int hashCode()
   {
      int result = super.hashCode();
      result = 31 * result + (countryCode != null ? countryCode.hashCode() : 0);
      result = 31 * result + (nationalDirectDialingPrefix != null ? nationalDirectDialingPrefix.hashCode() : 0);
      result = 31 * result + (areaCode != null ? areaCode.hashCode() : 0);
      result = 31 * result + (number != null ? number.hashCode() : 0);
      result = 31 * result + (extension != null ? extension.hashCode() : 0);
      return result;
   }
}
