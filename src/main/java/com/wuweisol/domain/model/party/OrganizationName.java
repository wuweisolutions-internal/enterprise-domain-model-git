/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.party;

import java.io.Serializable;

/**
 * @author Charles
 */
public class OrganizationName implements Serializable
{
   private static final long serialVersionUID = 1L;
   private Long id;
   private String name;
   private String use;

   public OrganizationName()
   {
   }

   public OrganizationName(String name)
   {
      this.name = name;
   }

   public OrganizationName(String name, String use)
   {
      this.name = name;
      this.use = use;
   }

   public Long getId()
   {
      return id;
   }

   public void setId(Long id)
   {
      this.id = id;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public String getUse()
   {
      return use;
   }

   public void setUse(String use)
   {
      this.use = use;
   }

   @Override
   public int hashCode()
   {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object)
   {
      // TODO: Warning - this method won't work in the case the id fields are not set
      if(!(object instanceof OrganizationName))
      {
         return false;
      }
      OrganizationName other = (OrganizationName) object;
      if((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      return "com.wuweisol.domain.model.party.OrganizationName[ id=" + id + " ]";
   }
}
