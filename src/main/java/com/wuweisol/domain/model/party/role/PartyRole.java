/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.party.role;

import com.wuweisol.domain.model.party.Party;

import java.io.Serializable;

/**
 * @author Charles
 */
public abstract class PartyRole<P extends Party> implements Serializable
{
   private static final long serialVersionUID = 1L;
   private Long id;
   private P party;

   protected PartyRole()
   {
   }

   protected PartyRole(P party)
   {
      this.party = party;
   }

   /**
    * Get the value of party
    *
    * @return the value of party
    */
   public P getParty()
   {
      return party;
   }

   /**
    * Set the value of party
    *
    * @param party new value of party
    */
   public void setParty(P party)
   {
      this.party = party;
   }

   public Long getId()
   {
      return id;
   }

   public void setId(Long id)
   {
      this.id = id;
   }

   @Override
   public int hashCode()
   {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object)
   {
      // TODO: Warning - this method won't work in the case the id fields are not set
      if(!(object instanceof PartyRole))
      {
         return false;
      }
      PartyRole other = (PartyRole) object;
      if((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      return "com.wuweisol.domain.model.party.relationship.PartyRole[ id=" + id + " ]";
   }

}
