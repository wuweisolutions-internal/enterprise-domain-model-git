/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.party.role;

import com.wuweisol.domain.model.party.Party;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Charles
 */
public class AssociationMember<P extends Party> extends PartyRole<P> implements Serializable {

   private static final long serialVersionUID = 1L;
   private Boolean active;
   private Date memberSince;
   private Date membershipExpires;

   /**
    * Get the value of active
    *
    * @return the value of active
    */
   public Boolean getActive()
   {
      return active;
   }

   /**
    * Set the value of active
    *
    * @param active new value of active
    */
   public void setActive(Boolean active)
   {
      this.active = active;
   }

   /**
    * Get the value of memberSince
    *
    * @return the value of memberSince
    */
   public Date getMemberSince()
   {
      return memberSince;
   }

   /**
    * Set the value of memberSince
    *
    * @param memberSince new value of memberSince
    */
   public void setMemberSince(Date memberSince)
   {
      this.memberSince = memberSince;
   }

   @Override
   public boolean equals(Object o)
   {
      if(this == o) return true;
      if(!(o instanceof AssociationMember)) return false;
      if(!super.equals(o)) return false;

      AssociationMember that = (AssociationMember) o;

      if(active != null ? !active.equals(that.active) : that.active != null) return false;
      if(memberSince != null ? !memberSince.equals(that.memberSince) : that.memberSince != null) return false;
      if(membershipExpires != null ? !membershipExpires.equals(that.membershipExpires) : that.membershipExpires != null)
         return false;

      return true;
   }

   @Override
   public int hashCode()
   {
      int result = super.hashCode();
      result = 31 * result + (active != null ? active.hashCode() : 0);
      result = 31 * result + (memberSince != null ? memberSince.hashCode() : 0);
      result = 31 * result + (membershipExpires != null ? membershipExpires.hashCode() : 0);
      return result;
   }

   public Date getMembershipExpires()
   {

      return membershipExpires;
   }

   public void setMembershipExpires(Date membershipExpires)
   {
      this.membershipExpires = membershipExpires;
   }

   @Override
   public String toString()
   {
      return "AssociationMember{" +
            "active=" + active +
            ", memberSince=" + memberSince +
            ", membershipExpires=" + membershipExpires +
            '}' + super.toString();
   }


}
