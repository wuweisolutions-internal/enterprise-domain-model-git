package com.wuweisol.domain.model.party;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 */
public class GeographicAddress
      extends Address
{
   protected List<String> addressLines = null;
   protected String city = null;
   protected String stateOrRegion = null;
   protected String zipOrPostalCode = null;
   protected String country = null;

   public GeographicAddress()
   {
   }

   /**
    * @return the addressLines
    */
   public List<String> getAddressLines()
   {
      return addressLines;
   }

   /**
    * @return the city
    */
   public String getCity()
   {
      return city;
   }

   /**
    * @return the country
    */
   public String getCountry()
   {
      return country;
   }

   /**
    * @return the stateOrRegion
    */
   public String getStateOrRegion()
   {
      return stateOrRegion;
   }

   /**
    * @return the zipOrPostalCode
    */
   public String getZipOrPostalCode()
   {
      return zipOrPostalCode;
   }

   /**
    * @param addressLines the addressLines to set
    */
   public void setAddressLines(List<String> addressLines)
   {
      this.addressLines = addressLines;
   }

   /**
    * @param addressLines
    */
   public void setAddressLines(String... addressLines)
   {
      this.setAddressLines(new ArrayList<String>(Arrays.asList(addressLines)));
   }

   public void setNextAddressLine(String addressLine)
   {
      if(addressLines == null)
      {
         addressLines = new ArrayList<String>();
      }
      if(addressLine != null && !addressLine.trim().isEmpty())
      {
         getAddressLines().add(addressLine.trim());
      }
   }

   /**
    * @param city the city to set
    */
   public void setCity(String city)
   {
      this.city = city;
   }

   /**
    * @param country the country to set
    */
   public void setCountry(String country)
   {
      this.country = country;
   }

   /**
    * @param stateOrRegion the stateOrRegion to set
    */
   public void setStateOrRegion(String stateOrRegion)
   {
      this.stateOrRegion = stateOrRegion;
   }

   /**
    * @param zipOrPostalCode the zipOrPostalCode to set
    */
   public void setZipOrPostalCode(String zipOrPostalCode)
   {
      this.zipOrPostalCode = zipOrPostalCode;
   }

   @Override
   public boolean equals(Object o)
   {
      if(this == o) return true;
      if(!(o instanceof GeographicAddress)) return false;
      if(!super.equals(o)) return false;

      GeographicAddress that = (GeographicAddress) o;

      if(addressLines != null ? !addressLines.equals(that.addressLines) : that.addressLines != null) return false;
      if(city != null ? !city.equals(that.city) : that.city != null) return false;
      if(country != null ? !country.equals(that.country) : that.country != null) return false;
      if(stateOrRegion != null ? !stateOrRegion.equals(that.stateOrRegion) : that.stateOrRegion != null) return false;
      if(zipOrPostalCode != null ? !zipOrPostalCode.equals(that.zipOrPostalCode) : that.zipOrPostalCode != null)
         return false;

      return true;
   }

   @Override
   public int hashCode()
   {
      int result = super.hashCode();
      result = 31 * result + (addressLines != null ? addressLines.hashCode() : 0);
      result = 31 * result + (city != null ? city.hashCode() : 0);
      result = 31 * result + (stateOrRegion != null ? stateOrRegion.hashCode() : 0);
      result = 31 * result + (zipOrPostalCode != null ? zipOrPostalCode.hashCode() : 0);
      result = 31 * result + (country != null ? country.hashCode() : 0);
      return result;
   }
}
