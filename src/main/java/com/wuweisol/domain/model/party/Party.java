/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.party;

import com.wuweisol.domain.model.property.StringProperty;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Charles
 */
public abstract class Party implements Serializable
{
   private static final long serialVersionUID = 1L;
   private Long id;
   private Map<String, StringProperty> stringProperties;
   private Set<PhoneNumber> phoneNumbers;

   public Party()
   {
   }

   public Map<String, StringProperty> getStringProperties()
   {
      if(stringProperties == null)
      {
         stringProperties = new HashMap<String, StringProperty>();
      }
      return stringProperties;
   }

   public void setStringProperties(Map<String, StringProperty> stringProperties)
   {
      this.stringProperties = stringProperties;
   }

   public Long getId()
   {
      return id;
   }

   public void setId(Long id)
   {
      this.id = id;
   }

   public Set<PhoneNumber> getPhoneNumbers()
   {
      return phoneNumbers;
   }

   public void setPhoneNumbers(Set<PhoneNumber> phoneNumbers)
   {
      this.phoneNumbers = phoneNumbers;
   }

   @Override
   public boolean equals(Object o)
   {
      if(this == o) return true;
      if(!(o instanceof Party)) return false;

      Party party = (Party) o;

      if(id != null ? !id.equals(party.id) : party.id != null) return false;
      if(phoneNumbers != null ? !phoneNumbers.equals(party.phoneNumbers) : party.phoneNumbers != null) return false;
      if(stringProperties != null ? !stringProperties.equals(party.stringProperties) : party.stringProperties != null)
         return false;

      return true;
   }

   @Override
   public int hashCode()
   {
      int result = id != null ? id.hashCode() : 0;
      result = 31 * result + (stringProperties != null ? stringProperties.hashCode() : 0);
      result = 31 * result + (phoneNumbers != null ? phoneNumbers.hashCode() : 0);
      return result;
   }

   @Override
   public String toString()
   {
      return "Party{" +
            "id=" + id +
            ", stringProperties=" + stringProperties +
            ", phoneNumbers=" + phoneNumbers +
            '}';
   }
}
