/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.party;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Charles
 */
public abstract class Address implements Serializable
{
   private static final long serialVersionUID = 1L;
   protected Long id;
   private String label;
   private Date validFrom;
   private Date validTo;

   public Long getId()
   {
      return id;
   }

   public void setId(Long id)
   {
      this.id = id;
   }

   @Override
   public boolean equals(Object o)
   {
      if(this == o) return true;
      if(!(o instanceof Address)) return false;

      Address address = (Address) o;

      if(id != null ? !id.equals(address.id) : address.id != null) return false;
      if(label != null ? !label.equals(address.label) : address.label != null) return false;
      if(validFrom != null ? !validFrom.equals(address.validFrom) : address.validFrom != null) return false;
      if(validTo != null ? !validTo.equals(address.validTo) : address.validTo != null) return false;

      return true;
   }

   @Override
   public int hashCode()
   {
      int result = id != null ? id.hashCode() : 0;
      result = 31 * result + (label != null ? label.hashCode() : 0);
      result = 31 * result + (validFrom != null ? validFrom.hashCode() : 0);
      result = 31 * result + (validTo != null ? validTo.hashCode() : 0);
      return result;
   }

   public String getLabel()
   {
      return label;
   }

   public void setLabel(String label)
   {
      this.label = label;
   }

   public Date getValidFrom()
   {
      return validFrom;
   }

   public void setValidFrom(Date validFrom)
   {
      this.validFrom = validFrom;
   }

   public Date getValidTo()
   {
      return validTo;
   }

   public void setValidTo(Date validTo)
   {
      this.validTo = validTo;
   }

   @Override
   public String toString()
   {
      return "Address{" +
            "id=" + id +
            ", label='" + label + '\'' +
            ", validFrom=" + validFrom +
            ", validTo=" + validTo +
            '}';
   }

}
