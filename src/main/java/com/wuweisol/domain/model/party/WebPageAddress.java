/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.party;

/**
 * @author Charles
 */
public class WebPageAddress
   extends Address
{
   private static final long serialVersionUID = 1L;
   protected String url = null;

   /**
    *
    */
   public WebPageAddress()
   {
      super();
   }

   public String getUrl()
   {
      return url;
   }

   public void setUrl(String url)
   {
      this.url = url;
   }

   @Override
   public boolean equals(Object o)
   {
      if(this == o) return true;
      if(!(o instanceof WebPageAddress)) return false;
      if(!super.equals(o)) return false;

      WebPageAddress that = (WebPageAddress) o;

      if(url != null ? !url.equals(that.url) : that.url != null) return false;

      return true;
   }

   @Override
   public int hashCode()
   {
      int result = super.hashCode();
      result = 31 * result + (url != null ? url.hashCode() : 0);
      return result;
   }

   @Override
   public String toString()
   {
      return "WebPageAddress{" +
            "url='" + url + '\'' +
            '}';
   }
}
