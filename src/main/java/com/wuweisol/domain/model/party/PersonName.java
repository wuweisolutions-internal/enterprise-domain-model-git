/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.party;

import java.io.Serializable;

/**
 * @author Charles
 */
public class PersonName implements Serializable
{
   private static final long serialVersionUID = 1L;
   private Long id;

   private String familyName;
   private String givenName;

   public PersonName()
   {
   }

   public PersonName(String givenName, String familyName)
   {
      this.givenName = givenName;
      this.familyName = familyName;
   }

   public Long getId()
   {
      return id;
   }

   public void setId(Long id)
   {
      this.id = id;
   }

   public String getFamilyName()
   {
      return familyName;
   }

   public void setFamilyName(String familyName)
   {
      this.familyName = familyName;
   }

   public String getGivenName()
   {
      return givenName;
   }

   public void setGivenName(String givenName)
   {
      this.givenName = givenName;
   }

   @Override
   public int hashCode()
   {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object)
   {
      // TODO: Warning - this method won't work in the case the id fields are not set
      if(!(object instanceof PersonName))
      {
         return false;
      }
      PersonName other = (PersonName) object;
      if((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      return "com.wuweisol.domain.model.party.PersonName[ id=" + id + " ]";
   }

}
