/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.party.role;

import com.wuweisol.domain.model.party.Organization;

import java.io.Serializable;

/**
 *
 * @author Charles
 */
public class MemberAssociation extends PartyRole<Organization> implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    @Override
    public String toString() {
        return "com.wuweisol.domain.model.party.relationship.MemberAssociation[ id=" + getId() + " ]";
    }
}
