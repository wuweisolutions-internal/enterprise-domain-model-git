/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.party;

/**
 * @author Charles
 */
public class Organization extends Party
{
   private static final long serialVersionUID = 1L;

   private OrganizationName organizationName;
   private String sortName;

   public Organization()
   {
   }

   public Organization(OrganizationName organizationName)
   {
      this.organizationName = organizationName;
   }

   public Organization(OrganizationName organizationName, String sortName)
   {
      this.organizationName = organizationName;
      this.sortName = sortName;
   }

   public Organization(String organizationName)
   {
      this(new OrganizationName(organizationName));
   }

   public Organization(String organizationName, String sortName)
   {
      this(new OrganizationName(organizationName), sortName);
   }

   public OrganizationName getOrganizationName()
   {
      return organizationName;
   }

   public void setOrganizationName(OrganizationName organizationName)
   {
      this.organizationName = organizationName;
   }

   /**
    * Get the value of sortName
    *
    * @return the value of sortName
    */
   public String getSortName()
   {
      return sortName;
   }

   /**
    * Set the value of sortName
    *
    * @param sortName new value of sortName
    */
   public void setSortName(String sortName)
   {
      this.sortName = sortName;
   }
}
