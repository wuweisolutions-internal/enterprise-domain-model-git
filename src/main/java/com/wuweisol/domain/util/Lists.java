/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.util;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Charles
 */
public class Lists
{
   public static interface Matcher<T>
   {
      boolean matches(T value);
   }
   
   public static <T> T head(List<T> source)
   {
      return source.get(0);
   }
   
   public static <T> T tail(List<T> source)
   {
      return source.get(source.size() - 1);
   }
   
   public static <T> T firstMatch(List<T> source, Matcher<T> matcher)
   {
      for (T candidate : source)
      {
         if (matcher.matches(candidate))
         {
            return candidate;
         }
      }
      return null;
   }
   
   public static <T> T lastMatch(List<T> source, Matcher<T> matcher)
   {
      ListIterator<T> iterator = source.listIterator(source.size());
      while (iterator.hasPrevious())
      {
         T candidate =  iterator.previous();
         if (matcher.matches(candidate))
         {
            return candidate;
         }
      }
      return null;
   }
   
   public static <T> List<T> allMatches(List<T> source, Matcher<T> matcher)
   {
      List<T> matches = new ArrayList<>(source.size());
      for (T candidate : source)
      {
         if (matcher.matches(candidate))
         {
            matches.add(candidate);
         }
      }
      return matches;
   }
}
