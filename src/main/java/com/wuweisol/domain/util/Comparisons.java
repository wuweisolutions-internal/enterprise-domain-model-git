/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.util;

/**
 *
 * @author Charles
 */
public class Comparisons
{

   private Comparisons()
   {
   }

   /**
    * Returns true only if all objects are null.
    *
    * @param values
    * @return
    */
   public static boolean allNull(Object... values)
   {
      boolean allNull = true;
      for (Object value : values)
      {
         allNull &= (value == null);
      }
      return allNull;
   }

   /**
    * Returns true only if all objects are not null.
    *
    * @param values
    * @return
    */
   public static boolean allNotNull(Object... values)
   {
      boolean allNotNull = true;
      for (Object value : values)
      {
         allNotNull &= (value != null);
      }
      return allNotNull;
   }

   /**
    * Returns true if all values are null or if all values are not null; that is, the null state of all objects must be the
    * same.
    *
    * @param values
    * @return
    */
   public static boolean nullStatesMatch(Object... values)
   {
      return (allNull(values) || allNotNull(values));
   }

   /**
    * Returns true only if both values are not null, value1 equals value2, and value2 equals value 1. (Invokes the equals method
    * from both values on the opposite value.)
    *
    * @param value1
    * @param value2
    * @return
    */
   public static boolean valuesPresentAndEqual(Object value1, Object value2)
   {
      return (allNotNull(value1, value2) && value1.equals(value2) && value2.equals(value1));
   }

   /**
    * Returns true if the null states of the objects are different or if both values are non-null but are not equal.
    *
    * @param value1
    * @param value2
    * @return
    */
   public static boolean valuesDiffer(Object value1, Object value2)
   {
      return (!nullStatesMatch(value1, value2) || !valuesPresentAndEqual(value1, value2));
   }
}
