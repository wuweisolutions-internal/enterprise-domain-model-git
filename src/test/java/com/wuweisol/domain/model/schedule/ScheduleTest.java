/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wuweisol.domain.model.schedule;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author Charles
 */
public class ScheduleTest
{
   private Schedule schedule;
   private final DateTime dateTime = new DateTime(2012, 9, 21, 10, 10);

   
   @BeforeClass
   public void createSchedule()
   {
      schedule = new Schedule();
      Interval interval = new Interval(dateTime, dateTime.plusMinutes(10));
      Appointment activity = new Appointment();
      activity.setInterval(interval);
      schedule.getAppointments().add(activity);
      
   }
   
   @Test
   public void scheduleIsNotNull()
   {
      assert schedule != null;
   }
   
   @Test
   public void earliestStartTimeIsEarliestStartTime()
   {
      assert schedule.getEarliestStartTime().equals(dateTime);
   }
   
}
